##  Multi migrate to Gitea

Multi migrate to Gitea is a python tool designed to clone repositories from (Gitlab, Gitlab and Bitbucket) to Gitea.


## Installation

```
git clone https://git.disroot.org/one_cluster/oc-multi_migrate_to_gitea.git
cd oc-multi_migrate_to_gitea
python setup.py install
```

## Recommended Python Version:

Currently supports **Python 3**.

* The recommended version for Python 3 is **3.6.x**

## Dependencies:

Depends on the `requests` and `click` python modules.


## Usage

Form                    | Description
------------------------| ----------------------------------------------------
list-repo               | List repositories from Gitlab, Bitbucket, Github Owner or Github Users 
list-repo-token         | List of Gitea repositories with the option of an access token and hosting domain
mirror-repo             | Create a mirror of an organization's or user's listed repositories to a specified Gitea server
get-branch-repo-gitea   | Gets if a certain branch exists in a repository

### Examples

* List an organization's repositories:

```migrate_to_gitea list-repo --provider_git github --team_org python  --verbose True```

* List an user's repositories only for Github:

```migrate_to_gitea list-repo --provider_git github_user --team_org python  --verbose True```

* List the repositories of an organization on a Gitea server with the option to include an access token :

``migrate_to_gitea list-repo-token --domain gitea.myserver.cyou --team_org my_team --access_token my_access_token --verbose True``

* Migrates all repositories of an organization or user to a specified Gitea server:

``migrate_to_gitea mirror-repo --domain gitea.myserver.cyou  --provider_git github --team_org_source git_team_or_user --access_token my_access_token --team_org_dest  my_team``
