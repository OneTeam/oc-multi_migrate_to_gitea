#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click==8.0.3',
                'requests==2.26.0']

setup(
    author="Alnus Tmp",
    author_email='alnus@disroot.org',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="Multi migrate git hosted to Gitea",
    py_modules=['migrate_to_gitea'],
    entry_points={
        'console_scripts': [
            'migrate_to_gitea=migrate_to_gitea:cli',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    long_description_content_type='text/x-rst',
    keywords='migrate, gitea',
    name='multimigrate_to_gitea',
    url='https://git.disroot.org/one_cluster/oc-multi_migrate_to_gitea',
    version='0.1.0'
)
